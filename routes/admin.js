/**
 * npm modules import
*/
var express = require('express');
var async = require('async');
var bodyParser = require('body-parser');

/**
 * package modules import
*/
var models = require('../models.js');
var knex = require('../config/dbSetup.js');
var lib = require('../lib/index.js');
var helpers = lib.helpers;

/**
 * express router setup
*/
var router = express.Router();
router.use(bodyParser.urlencoded({ extended: false })) 
router.use(bodyParser.json()) 


router.post('/imageGroup', function(req, res){
	console.log(req.body)
	if(req.body == undefined || req.body == null) {
		return res.status(400).send('Invalid Request!');
	} else {
		var validationResult = helpers.validateRequest(req.body, models.image_groups);
		if(!validationResult.valid) {
			return res.status(400).send({error: helpers.errorBuilder(validationResult.details)});
		} else {
			knex('image_groups')
				.returning(Object.keys(models.image_groups))
				.insert(validationResult.request)
				.then(function(result){
					res.status(200).send(result);
				})
				.catch(function(err){
					res.status(500).send(err);_
				});
		}
	}
});

router.post('/sizeClass', function(req, res){
	if(req.body == undefined || req.body == null) {
		return res.status(400).send('Invalid Request!');
	} else {
		var validationResult = helpers.validateRequest(req.body, models.image_sizes);
		if(!validationResult.valid) {
			return res.status(400).send({error: helpers.errorBuilder(validationResult.details)});
		} else {
			knex('image_sizes')
				.returning(Object.keys(models.image_groups))
				.insert(validationResult.request)
				.then(function(result){
					res.status(200).send(result);
				})
				.catch(function(err){
					res.status(500).send(err);_
				});
		}	
	}
});

router.post('/groupSizes', function(req, res){
	if(req.body == undefined || req.body == null) {
		return res.status(400).send('Invalid Request!');
	} else {
		var validationResult = helpers.validateRequest(req.body, models.group_sizes);
		if(!validationResult.valid) {
			return res.status(400).send({error: helpers.errorBuilder(validationResult.details)});
		} else {
			async.parallel({
				imageGroup: function(callback){
					helpers.checkForeignKey('image_groups', 'image_group_id', validationResult.request.image_group_id, function(code){
						callback(null, code);
					});
				},
				imageSize: function(callback){
					helpers.checkForeignKey('image_sizes', 'image_size_id', validationResult.request.image_size_id, function(code){
						callback(null, code);
					});
				}
			}, function(err, results){
				console.log('proceeding')
				console.log(results)
				if(results.imageGroup == 500 || results.imageSize == 500) {
					return res.status(500).send('Internal Server Error! Please try again');
				} else {
					var invalid = false;
					var details = {};
					if(results.imageGroup == 400) { 
						invalid = true;
						details.image_group_id = 'Image_group with this id does not exist!!'
					}
					if(results.imageSize == 400) {
						invalid = true; 
						details.image_size_id = 'Image_size with this id does not exist!!'
					}
					if(invalid) {
						return res.status(400).send({error: helpers.errorBuilder(details)});
					} else {
						console.log('executing insert')
						knex('group_sizes')
							.returning(Object.keys(models.image_groups))
							.insert(validationResult.request)
							.then(function(result){
								return res.status(200).send(result);
							})
							.catch(function(err){
								return res.status(500).send(err);
							});
					}	
				}
			});
		}	
	}
});

router.get('/imageGroups', function(req, res){
	knex('image_groups')
		.select()
		.then(function(image_groups){
			return res.status(200).send(image_groups);
		})
		.catch(function(err){
			return res.status(500).send(err);
		});
});


module.exports = router;