/**
	* load environment variables
**/
if(process.env.NODE_ENV == 'production')
	require('dotenv').config({path: './env/production/.env'})
if(process.env.NODE_ENV == 'staging')
	require('dotenv').config({path: './env/staging/.env'})
if(process.env.NODE_ENV == 'integration')
	require('dotenv').config({path: './env/integration/.env'})

/*
   **express router Setup
*/
var express = require('express');
var router = express.Router();

/*
   **NPM modules import
*/
var AWS = require('aws-sdk');
var bodyParser = require('body-parser');
var async = require('async');
var fs = require('fs');
var formidable = require('formidable');
var _ = require('lodash');
var bluebird = require('bluebird');
/*
   **LOCAL modules import
*/
var DB_DUMP = require('../config/config.js').config;
var lib = require('../lib/index.js');
var processingSetup = lib.processingSetup;
var imageQC = lib.imageQC;
var helpers = lib.helpers;
var imageCompressor = lib.imageCompressor;
var imageResizer = lib.imageResizer;
var imageUploader = lib.imageUploader;

/*
   **AWS Setup
*/
AWS.config.region = process.env.AWS_REGION;
AWS.config.update({ 
	accessKeyId: process.env.AWS_KEY_ID, 
	secretAccessKey: process.env.AWS_SECRET_KEY
});

/*
   **AWS Bucket Setup
*/
var bucket = new AWS.S3({ params: { 
    Bucket: process.env.AWS_BUCKET 
} });

router.use(bodyParser.json({limit: '50mb'}));
router.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

/*
   ** Image Processing API
*/
router.post('/upload/:groupName', function(req, res, next){
	console.log('AWS : ======>>>>>>>>>>>>request received for upload api to '+req.params.groupName);

	/*
	   **formidable incoming form setup
	*/
	var form = new formidable.IncomingForm();
	form.uploadDir = "./bufferImages";
	form.keepExtensions = true;

	/*
       **parse the request using formidable
    */
	form.parse(req);
	
	/*
	   **incoming form new file receiving started event
	*/
	form.on('fileBegin', function(name, file) {
		console.log('received file: '+file.name);
		/*
		   **set the local path to save the file after parsing
		*/
        file.path = './'+process.env.BUFFER_DIRECTORY+'/'+file.name;
        
    });

	/*
	   **incoming form end event
	*/
    form.on('end', function(name, file) {
    	/*
		   **generate the filename by which the file will be uploaded to S3
		*/
		var mime_type = helpers.getMimeType(this.openedFiles[0].path);
        var temp = this.openedFiles[0].name.split('.');
		var index = (temp.length-1);
		temp[index] = mime_type;
		var fileName = temp.join('.');
		/*
		   **generate the url of the original file
		*/
		var originalFileUrl = process.env.AWS_S3_URL+'/'+process.env.AWS_BUCKET+'/product_images-originals/'+req.params.groupName+'/'+fileName;	
		var originalFileUploadFolder = process.env.AWS_BUCKET+'/product_images-originals/'+req.params.groupName;
		/*
           **reload settings from the database
        */
        DB_DUMP = require('../config/config.js').config;
       	/*
		   **filter DB_DUMP specially for the requested group
       	*/
       	var PROCESSING_OPTIONS = processingSetup(DB_DUMP, req.params.groupName, fileName);
       	/*
    	   **check if the group uploading to is registered
        */
        //console.log('PROCESSING_OPTIONS')
        //console.log(PROCESSING_OPTIONS)
    	if(!PROCESSING_OPTIONS.validGroup) {
    		console.log('The requested group is not a valid one!!');
      		/*
			   **delete file from local storage
      		*/
      		fs.unlink(this.openedFiles[0].path, function(err){
      			if(err) {
      				console.log('error deleting file from local storage');
      			} else {
      				console.log('File Successfully deleted from local storage')
      			}	
      		});
      		/*
			   **the group uploading is not registered
    		*/
    		var error = {};
      		error.message = 'Sorry!! The group you are trying to upload to is not registered!!';
    		return res.status(400).send({error: error});
    	} else {
    		console.log('the Requested group is a valid one!!')
    		/*
			   **the group uploading to is registered
    		*/
    		/*
		       **check if the image uploaded is with acceptable parameters
			*/
    		var qualityCheck = imageQC(PROCESSING_OPTIONS.groupData, this.openedFiles[0].path);
    		if(!qualityCheck.status) {
    			/*
			   	   **delete file from local storage
	      		*/
	      		fs.unlink(this.openedFiles[0].path, function(err){
	      			if(err) {
	      				console.log('error deleting file from local storage');
	      			} else {
	      				console.log('File Successfully deleted from local storage')
	      			}	
	      		});
	      		/*
				   **the uploaded image is not with proper parameters
    			*/
				var error = {};
	      		error.message = 'Sorry!! The Image you are trying to upload is not acceptable!!';
	      		error.details = qualityCheck.details;
	    		return res.status(400).send({error: error});	
    		} else {
    			
    			console.log('The image is of proper quality!!');
    			console.log('Proceeding.....')
    			
    			/*
				   **the image uploaded is with proper parameters
				   **the group uploading to is registered
    			*/
	    		
	    		/*
				   **save a reference to this
	    		*/
	    		var formReference = this;
	    		
	    		/*
				   **upload the original Image to S3
	    		*/
				bucket.putObject({
			        Bucket: originalFileUploadFolder,
			        Key: fileName,
			        Body: fs.createReadStream(formReference.openedFiles[0].path)
		      	}, function(err, data) {
		      		/*
					   **delete file from local storage
		      		*/
		      		fs.unlink(formReference.openedFiles[0].path, function(err){
		      			if(err) {
		      				console.log('error deleting original file from local storage');
		      			} else {
		      				console.log('Original File Successfully deleted from local storage')
    						console.log('Proceeding.....')
		      			}	
		      		});

			        if(err) {
			        	console.log('Error in uploading original file to S3:'+err);
			        	return res.status(500).send({error: err});
			        }
			        else {
			        	
			        	console.log('Original file Successfully uploaded to S3 Bucket.');
    					console.log('Proceeding.....')
			      		
			      		/**
						 * call tiny png service to compress the image
			      		*/
			      		imageCompressor(originalFileUrl, PROCESSING_OPTIONS.convertSizes, function(err, compressedImageBuffer){
	                		if(err) {
	                			return res.status(500).send({error: err});	
	                		} else {	
	                			
	                			console.log('Original Image Successfully compressed')
								console.log('Proceeding.....')
	                			
	                			/**
								 * call image resizer service to resize image
	                			*/
	                			imageResizer(compressedImageBuffer, PROCESSING_OPTIONS.convertSizes)
	                				.then(function(paramsWithImage){

	                					console.log('Compressed Image Successfully Resized')
										console.log('Proceeding.....')
	                					
	                					imageUploader(paramsWithImage)
	                						.then(function(result){
	                							
	                							console.log('Resized Images Successfully Uploaded to S3')
												console.log('Finishing.....')
	                							
	                							return res.status(200).send(result)
	                						})
	                						.catch(function(err){
	                							return res.status(500).send({error: err});
	                						});
	                				})
	                				.catch(function(err){
	                					return res.status(500).send({error: err});	
	                				});
	                		}
	                	});
			        }
			    });
    		}
    	}	
    });
});


module.exports = router;