/**
 * npm modules import
*/
var express = require('express');
var async = require('async');
var path = require('path');
var bodyParser = require('body-parser');

/**
 * package modules import
*/
var models = require('../models.js');
var knex = require('../config/dbSetup.js');
var lib = require('../lib/index.js');
var helpers = lib.helpers;

/***
 * express router setup
*/
var router = express.Router();
router.use(bodyParser.urlencoded({ extended: false })) 
router.use(bodyParser.json())


router.get('/', function(req, res){
	res.sendFile(path.resolve(__dirname+'/../client/template.html'));	
});

router.use('/admin', require('./admin.js'));
router.use('/services', require('./service.js'));

module.exports = router;