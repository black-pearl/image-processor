module.exports = {
	image_groups: {
		image_group_id		: 	'number',
		group_name			: 	'string',
		max_upload_height	:	'number', 
		max_upload_width	: 	'number',
		allowed_formats		:	'string' 
	},
	image_sizes: {
		image_size_id 		: 	'number',
		alias				: 	'string',	
		width				: 	'number',		
		height				: 	'number'		
	},
	group_sizes: {
		group_sizes_id		: 	'number',
		image_group_id		:	'number',		
		image_size_id		: 	'number',
		device_type			:   'string'
	}
}