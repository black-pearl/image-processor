/**
	* load environment variables
**/
if(process.env.NODE_ENV == 'production')
	require('dotenv').config({path: './env/production/.env'})
if(process.env.NODE_ENV == 'staging')
	require('dotenv').config({path: './env/staging/.env'})
if(process.env.NODE_ENV == 'integration')
	require('dotenv').config({path: './env/integration/.env'})

/**
 * import npm modules
*/
var sharp = require('sharp');
var bluebird = require('bluebird');

module.exports = function(imageBuffer, params){
	var promises = [];
	
	console.log('params for resizing image to: ');
	console.log(params)

	params.map(function(param){
		promises.push(sharp(imageBuffer).resize(param.width, param.height).toBuffer());	
	});

	return new bluebird(function(resolve, reject){
		Promise
			.all(promises)
			.then(function(results){
				for(var i=0;i<results.length;i++) {
					params[i].image = results[i]
				}
				return resolve(params);
			})
			.catch(function(err){
				console.log('Error in resizing compressed image: ');
				console.log(err)
				return reject(err);
			});
	});
};