/**
	* load environment variables
**/
if(process.env.NODE_ENV == 'production')
	require('dotenv').config({path: './env/production/.env'})
if(process.env.NODE_ENV == 'staging')
	require('dotenv').config({path: './env/staging/.env'})
if(process.env.NODE_ENV == 'integration')
	require('dotenv').config({path: './env/integration/.env'})

/*
   **LOCAL modules import
*/
var config = require('../config/config.js');

/*
   **NPM modules import
*/
var async = require('async'); 
var fs = require('fs');

module.exports = function(){
	/*
	   **check for existence of buffer folder 
	*/
	if(!fs.existsSync(__dirname+'/../'+process.env.BUFFER_DIRECTORY)) {
		/*
		   **folder does not exist
		   **create folder
		*/
		fs.access(__dirname+'/../', fs.W_OK, function(err){
			if(err) {
				console.log('\ndo not have write permissions in root directory!');
    			console.log('\nplease change permissions and run again\n');
    			process.kill(process.pid);
			} else {
				fs.mkdirSync(__dirname+'/../'+process.env.BUFFER_DIRECTORY);
				/*
				   **if there are no write permissions to the folder then stop process and asks for permissions
				*/	
				fs.access(__dirname+'/../'+process.env.BUFFER_DIRECTORY, fs.W_OK, function(err) {
					/*
					   **there are no write permissions to buffer folder
					*/
					if(err){
						console.log(err)
				    	console.log('\ndo not have write permissions to '+process.env.BUFFER_DIRECTORY);
				    	console.log('\nplease change permissions and run again\n');
				    	process.kill(process.pid);
				  	}			
				});	
			}
		});
	} else {
		/*
		   **if there are no write permissions to the folder then stop process and asks for permissions
		*/	
		fs.access(__dirname+'/../'+process.env.BUFFER_DIRECTORY, fs.W_OK, function(err) {
			/*
			   **there are no write permissions to buffer folder
			*/
			if(err){
				console.log(err)
		    	console.log('\ndo not have write permissions to '+process.env.BUFFER_DIRECTORY);
		    	console.log('\nplease change permissions and run again\n');
		    	process.kill(process.pid);
		  	}			
		});
	}			
	
	/*
	   **load settings from database 
	*/
	config.configLoader(function(){
		console.log('config successfully loaded from database!!');
		console.log(config.config)
		console.log('image prrocessing server listening on port: '+process.env.PORT);
	});
};