module.exports = {
	boot: require('./boot.js'),
	helpers: require('./helpers.js'),
	imageCompressor: require('./imageCompressor.js'),
	imageResizer: require('./imageResizer.js'),
	imageUploader: require('./imageUploader.js'),
	imageQC: require('./imageQC.js'),
	processingSetup: require('./processingSetup.js')
};