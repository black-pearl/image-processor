/*
   **NPM modules import
*/
var validator = require('validator');
var fileType = require('file-type');
var fs = require('fs');
/*
   **LOCAL modules import
*/
var models = require('../models.js');
var knex = require('../config/dbSetup.js');

module.exports = {
	/*
	   **read mime type of the file
	*/
	getMimeType: function(path){
		var fileParams = fileType(fs.readFileSync(path));
		if(fileParams == null) {
	    	return null;
	    } else {
	    	var temp = fileParams.mime.split('/');
		    var index = (temp.length-1);
		    var mime_type = temp[index];
		    return mime_type;
	    }
	},
	/*
       **validate API request object  
	*/
	validateRequest: function(request, model){
		var requestedKeys = Object.keys(request);				//the keys present in the request object
		var validKeys = Object.keys(model);						//keys that are valid
		var invalid = false;
		var details = {};
		requestedKeys.forEach(function(key){
			/*
			   **check if the current key is a valid one or not
			*/
			if(validKeys.indexOf(key) == -1) {
				/*
				   **the key is not a valid one 
				*/
				invalid = true;
				details[key] = 'Invalid Field';
			} else {
				/*
				   **the key is a valid one 
				*/
				/*
				   **check if the type of the value of the key is matching the type mentioned in the schema 
				*/
				if(typeof request[key] != model[key]) {
					invalid = true;
					details[key] = 'Invalid Type';
				} else {
					if(typeof request[key] == 'string') {
						request[key] = validator.trim(request[key])
						request[key] = validator.escape(request[key]);
					}
				}
			}
		});
		return {
			valid: !(invalid),
			request: request,
			details: details
		};
	},
	/*
	   **check for existence of Foreign Key 
	*/
	checkForeignKey: function(model, modelPK, id, callback){
		var where = {};
		where[modelPK] = id;
		knex(model)
			.where(where)
			.select()
			.then(function(result){
				console.log('inside FKcheck')
				console.log(result)
				if(result.length == 0) {
					callback(400);
				} else {
					callback(200);
				}
			})
			.catch(function(error){
				console.log('fetch error: '+error)
				callback(500);
			})
		console.log('FK helper completed')
	},
	/*
	   **error object builder
	*/
	errorBuilder: function(details){
		var error = {};
		error.details = details;
		return error;
	}
}