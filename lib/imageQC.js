/*
   **NPM modules import
*/
var imageSize = require('image-size');
var fileType = require('file-type');
var fs = require('fs');
var helpers = require('./helpers.js');
/*
   **MIME type check of the image
*/
var mimeTypeCheck = function(options, path){
	/*
	   **get mime type of the image file
	*/
    var mime_type = helpers.getMimeType(path);
    console.log('MIME-TYPE of the uploaded image: '+mime_type)
    /*
	   **check if the mime type of the file is allowed
    */
    if(mime_type == null || (options.allowed_formats.indexOf(mime_type) == -1)) {
    	return false;
    } else {
    	return true;
    }
};

/*
   **Size check of the image
*/
var sizeCheck = function(options, path){
	//read dimensions of the image
    var dimensions = imageSize(path);
    console.log('Dimensions of the uploaded image: ')
    console.log(dimensions)
    if((dimensions.width <= options.group_max_width) && (dimensions.height <= options.group_max_height)) {
    	return true;
    } else {
    	return false;
    }
};

module.exports = function(options, path){ 
	var invalid = false;
	var details = {};
	if(!mimeTypeCheck(options, path)) {
		console.log('The mime_type of the image is not acceptable!!');
		invalid = true;
		details.mimeType = 'Images with this Mime-Type are not allowed';
	} else {
		if(!sizeCheck(options, path)) {
			console.log('The image is not within allowed dimensions!!'); 
			invalid = true;
			details.size = 'The Image is not within allowed dimensions';
		}
	}
	
	if(invalid) {
		return {
			status: false,
			details: details
		}
	} else {
		return {
			status: true
		}
	}
};