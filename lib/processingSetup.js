/**
	* load environment variables
**/
if(process.env.NODE_ENV == 'production')
	require('dotenv').config({path: './env/production/.env'})
if(process.env.NODE_ENV == 'staging')
	require('dotenv').config({path: './env/staging/.env'})
if(process.env.NODE_ENV == 'integration')
	require('dotenv').config({path: './env/integration/.env'})

module.exports = function(PROCESSING_OPTIONS, groupName, fileName){
	var groupSizes = [];
	/*
	   **generate the filename for device_type mobile
	*/
	var literals = fileName.split('.');
	literals[0] += process.env.MOBILE_EXTENSION;
	mobileFilename = literals.join('.');
	PROCESSING_OPTIONS.forEach(function(option){
		if(option.group_name == groupName) {
			groupSizes.push(option);
		}
	});
	if(groupSizes.length == 0) {
		return {
			validGroup: false,
			groupData: null,
			convertSizes: []
		};
	} else {
		var processingParams = [];
		groupSizes.forEach(function(group){
			if(group.width != undefined && group.height != undefined) {
				if(group.device_type == 'web') {
					processingParams.push({
						path: process.env.AWS_BUCKET+'/product_images-'+group.device_type+'/'+groupName+'/'+group.alias,
						width: group.width,
						fileName: fileName,
						height: group.height,
						alias: group.alias,
						device_type: group.device_type 	
					});
				} else {
					processingParams.push({
						path: process.env.AWS_BUCKET+'/product_images-'+group.device_type+'/'+groupName+'/'+group.alias,
						width: group.width,
						fileName: mobileFilename,
						height: group.height,
						alias: group.alias,
						device_type: group.device_type 	
					});
				}	
			}	
		});
		return {
			validGroup: true,
			groupData: {
				group_name: groupSizes[0].group_name,
				group_id: groupSizes[0].image_group_id,
				group_max_width: groupSizes[0].max_upload_width,
				group_max_height: groupSizes[0].max_upload_height,
				allowed_formats: groupSizes[0].allowed_formats.split(',')
			},
			convertSizes: processingParams
		};
	}
}