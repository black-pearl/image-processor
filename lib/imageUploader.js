/**
	* load environment variables
**/
if(process.env.NODE_ENV == 'production')
	require('dotenv').config({path: './env/production/.env'})
if(process.env.NODE_ENV == 'staging')
	require('dotenv').config({path: './env/staging/.env'})
if(process.env.NODE_ENV == 'integration')
	require('dotenv').config({path: './env/integration/.env'})

/**
 * import npm modules
*/
var AWS = require('aws-sdk');
var bluebird = require('bluebird')

/*
   **AWS Setup
*/
AWS.config.region = process.env.AWS_REGION;
AWS.config.update({ 
	accessKeyId: process.env.AWS_KEY_ID, 
	secretAccessKey: process.env.AWS_SECRET_KEY
});

/*
   **AWS Bucket Setup
*/
var bucket = new AWS.S3({ params: { 
    Bucket: process.env.AWS_BUCKET 
} });

module.exports = function(params){
	var promises = [];
	
	params.map(function(param){
		promises.push(new bluebird(function(resolve, reject){
			bucket.upload({
				Body: param.image,
				ContentType: 'image',
				ContentLength: param.image.byteLength,
				Bucket: param.path,
				Key: param.fileName
			}, function(err, data) {
				if (err) {
					reject(err);
				}
				else {
					param.s3Info = data;
					resolve(param);
				}
			});
		}));
	});

	return new bluebird(function(resolve, reject){
		Promise
			.all(promises)
			.then(function(results){
				var response = {
    				message: 'Processing Successful!!'
    			};
    			response.images = {};
    			response.images.web = {};
    			response.images.mobile = {};
    			var temp;
    			results.map(function(data){
    				temp = {};
    				temp[data.alias] = data.s3Info.Location;
    				response['images'][data.device_type] = Object.assign(response['images'][data.device_type], temp);
				});
				return resolve(response);
			})
			.catch(function(err){
				console.log('Error in uploading resized image to S3: ');
				console.log(err)
				return reject(err);
			});
	});
};