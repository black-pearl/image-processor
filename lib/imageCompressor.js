/**
	* load environment variables
**/
if(process.env.NODE_ENV == 'production')
	require('dotenv').config({path: './env/production/.env'})
if(process.env.NODE_ENV == 'staging')
	require('dotenv').config({path: './env/staging/.env'})
if(process.env.NODE_ENV == 'integration')
	require('dotenv').config({path: './env/integration/.env'})

var tinyPng = require('tinify');

/*
   ** tiny Png BOOT-UP
*/
tinyPng.key = process.env.TINYPNG_API_KEY;
tinyPng.validate(function(err){
	if(err) {
		console.log('Error in validating Tiny Png API Key')
		console.log('!!!!!!!!!!!!!Correct API key and restart server')
	} else {
		console.log('Tiny Png API key successfully validated!!')
	}
});

module.exports = function(originalFileUrl, params, callback){
	/*
	   **create a source from original image uploaded on S3
	*/
	var source = tinyPng.fromUrl(originalFileUrl);

	source.toBuffer(function(err, result){
		if(err) {
			console.log('Error in compressing original image: ')
			console.log(err)
			callback(err, null)
		} else {
			callback(null, result);
		}
	});
	//	/*
	//	   **resize the image to proper size
	//	*/
	//	var resizedSource = source.resize({
	//	  	method: "fit",
	//	  	width: params.width,
	//	  	height: params.height
	//	});
	//
	//  /*
	//	   **store the resized image to S3 in proper directory
	//  */
	//	resizedSource.store({
	//		service: "s3",
	//		aws_access_key_id: process.env.AWS_KEY_ID,
	//		aws_secret_access_key: process.env.AWS_SECRET_KEY,
	//		region: process.env.AWS_REGION,
	//		path: params.path				//example  ---- "example-bucket/my-images/optimized.jpg"
	//	}).location(function(err){
	//		if(err) {
	//			return callback(err, null);
	//		} else {
	//			var response = {};
	//			response.device_type = params.device_type;
	//			response.url = {};
	//			response['url'][params.alias] = process.env.AWS_S3_URL+'/'+params.path;
	//			return callback(null, response);
	//		}
	//  });
};    