/**
 * load environment variables
**/
if(process.env.NODE_ENV == 'production')
	require('dotenv').config({path: './env/production/.env'})
if(process.env.NODE_ENV == 'staging')
	require('dotenv').config({path: './env/staging/.env'})
if(process.env.NODE_ENV == 'integration')
	require('dotenv').config({path: './env/integration/.env'})

/*
 * express app setup
*/
var express = require('express');
var app = express();

/*
 * LOCAL modules import
*/
var lib = require('./lib/index.js');
var boot = lib.boot;

/*
 * NPM modules import
*/
var path = require('path');

/*
 * routes setup
*/
app.use('/', require('./routes/index.js'));

/**
 * Start the server 
**/
if(process.env.NODE_ENV == undefined) {
	/*
	 * If environment to be started on is not provided
	*/
	console.log('Please mention environment and restart server!!');
} else {
	app.listen(process.env.PORT, function(){
		boot();
	});
}