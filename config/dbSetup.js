/*
 * create a knex object
*/
var knex = require('knex')({
    client: 'mysql',
    connection: {
        charset  : 'utf8',
        host: process.env.DB_HOST,
	    user: process.env.DB_USER,
		password: process.env.DB_PASSWORD,
		database: process.env.DB_NAME
	}
});

/*
 * create the tables if they do not exist
*/
knex.schema.createTableIfNotExists('image_groups', function(table) {
    table.increments('image_group_id');
    table.string('group_name').notNullable().unique();
    table.integer('max_upload_height').notNullable();
    table.integer('max_upload_width').notNullable();
    table.string('allowed_formats').notNullable();
}).createTableIfNotExists('image_sizes', function(table) {
    table.increments('image_size_id');
    table.string('alias').notNullable();
    table.integer('width').unsigned().notNullable();
    table.integer('height').unsigned().notNullable();
    table.unique(['width', 'height']);
}).createTableIfNotExists('group_sizes', function(table) {
    table.increments('group_sizes_id');
    table.integer('image_group_id').unsigned().references('image_groups.image_group_id').notNullable();
    table.integer('image_size_id').unsigned().references('image_sizes.image_size_id').notNullable();
    table.string('device_type').notNullable();
    table.unique(['image_group_id', 'image_size_id', 'device_type']);
}).catch(function(err){
	  console.log('error: '+err)
});

module.exports = knex;
