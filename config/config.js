/*
 * LOCAL modules import
*/
var knex = require('./dbSetup.js');
var models = require('../models.js');

/*
 * NPM modules import
*/
var async = require('async');

var CONFIG;
var ERR_LOADING_CONFIG = true;


var configLoader = function(callback){
	/*
	 * keep trying to load the settings from database until it is loaded without errors  
	*/
	async.doUntil(
		function(cb){
			knex('image_groups')
				.innerJoin('group_sizes', 'image_groups.image_group_id', 'group_sizes.image_group_id')
				.innerJoin('image_sizes', 'group_sizes.image_size_id', 'image_sizes.image_size_id')
				.then(function(image_groups){
					CONFIG = image_groups;
					module.exports.config = CONFIG;
					ERR_LOADING_CONFIG = false;
					cb();
				})
				.catch(function(err){
					ERR_LOADING_CONFIG = true;
					cb();
				});
		},
		function(){
			/*
			 * if error occurs re-iterate
			 * else continue
			*/
			if(ERR_LOADING_CONFIG) {
				return false;
			} else {
				return true;
			}
		},
		function(){
			callback();
		}
	);
};

module.exports = {
	config: CONFIG,
	configLoader: configLoader
}